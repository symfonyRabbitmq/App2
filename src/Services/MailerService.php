<?php

namespace App\Services;

use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class MailerService
{
    private MailerInterface $mailer;

    public function __construct(
        MailerInterface $mailer
    )
    {
        $this->mailer = $mailer;
    }

    public function sendMail(
        string $to,
        string $template,
        string $subject,
        array $context,
        string $cc=null

    )
    {

        $email = (new TemplatedEmail())
            ->to(new Address($to))
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context)
        ;

        if(!is_null($cc)) $email->cc($cc);
        
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface  $e) {
            //throw $th;
        }

    }
}
