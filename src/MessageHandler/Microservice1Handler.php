<?php

namespace App\MessageHandler;

use App\Services\MailerService;
use App\Message\Service1Message;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


class Microservice1Handler implements MessageHandlerInterface
{
    public function __construct(
        MailerService $mailerService
    )
    {
        $this->service = $mailerService;
    }

    public function __invoke(Service1Message $message)
    {
        $this->service->sendMail(
            "romeokamgo@gmail.com",
            "Emailtemplate/Notification.html.twig",
            "test rabbitmq",
            []
        );
    }
}
