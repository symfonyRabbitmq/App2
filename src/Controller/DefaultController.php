<?php

namespace App\Controller;

use App\Message\Service1Message;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class DefaultController extends AbstractController
{

    public function __construct(
        MessageBusInterface $bus
    )
    {
        $this->bus = $bus;       
    }

    #[Route('/',name:"index", methods:'GET')]
    public function index(): Response
    {

        $notify = new Service1Message();
        $this->bus->dispatch($notify);

        return new Response(
            '<html><body>Microservie 2 </body></html>'
        );
    }
}
